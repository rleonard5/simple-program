#include <iostream>

/* This is a little example of how to use the value-less enums in c++ */

enum class Color { red, green, blue  };

template < Color c >
struct A {
};

template<>
struct A<Color::red> {
  static const Color color = Color::red;

  void fun() {
    std::cout << "A<red>" << std::endl;
  }
};

int main() {
  A<Color::red> a;
  a.fun();
  std::cout << "sizeof(A<red>): " << sizeof(a) << std::endl;
  if (a.color == Color::red) {
    std::cout << "whoa red" << std::endl;
  }
  return 0;
}
